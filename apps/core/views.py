from django.shortcuts import render
from django.contrib.auth.models import User
from rest_framework import permissions
from rest_framework import renderers
from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from apps.core.models import Video, PlayList, VideoPlayList
from apps.core.serializers\
    import VideoSerializer, PlayListSerializer, VideoPlayListSerializer

class VideoViewSet(viewsets.ModelViewSet):
    queryset = Video.objects.all()
    serializer_class = VideoSerializer

class PlayListViewSet(viewsets.ModelViewSet):
    queryset = PlayList.objects.all()
    serializer_class = PlayListSerializer

class VideoPlayListViewSet(viewsets.ModelViewSet):
    queryset = VideoPlayList.objects.all()
    serializer_class = VideoPlayListSerializer
