from django.utils import timezone
from django.db import models



class Album(models.Model):
    album_name = models.CharField(max_length=100)
    artist = models.CharField(max_length=100)

class Track(models.Model):
    album = models.ForeignKey(Album, related_name='tracks')
    order = models.IntegerField()
    title = models.CharField(max_length=100)
    duration = models.IntegerField()

    class Meta:
        unique_together = ('album', 'order')
        #order_by = ['order']
        ordering = ['order']


    def __str__(self):
        return '%d: %s' % (self.order, self.title)


class Video(models.Model):
    video_name = models.CharField(max_length=25)
    url = models.CharField(max_length=50)

    def __str__(self):
        return self.video_name


class PlayList(models.Model):
    playListDate = models.DateTimeField()
    videos = models.ManyToManyField(Video, through='VideoPlayList', related_name='playLists')


class VideoPlayList(models.Model):
    video = models.ForeignKey(Video)
    playList = models.ForeignKey(PlayList)
    status = models.CharField(max_length=1)

    def __str__(self):
        return self.status
