from rest_framework import serializers
from apps.core.models import Video, PlayList, VideoPlayList


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ('id', 'video_name', 'url')


class PlayListSerializer(serializers.ModelSerializer):
    videos = VideoSerializer(many=True)

    class Meta:
        model = PlayList
        fields = ('videos',)


class VideoPlayListSerializer(serializers.HyperlinkedModelSerializer ):
    playlist_id = serializers.ReadOnlyField(source='playList.id')
    playlist_date = serializers.ReadOnlyField(source='playList.playListDate')
    video_id = serializers.ReadOnlyField(source='video.id')
    video_name = serializers.ReadOnlyField(source='video.video_name')
    video_url = serializers.ReadOnlyField(source='video.url')


    class Meta:
        model = VideoPlayList
        fields = ('playlist_id', 'playlist_date', 'video_id', 'video_name', 'video_url')

