# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PlayList',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('playListDate', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('video_name', models.CharField(max_length=25)),
                ('url', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='VideoPlayList',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('status', models.CharField(max_length=1)),
                ('playList', models.ForeignKey(to='core.PlayList')),
                ('video', models.ForeignKey(to='core.Video')),
            ],
        ),
        migrations.AddField(
            model_name='playlist',
            name='playLists',
            field=models.ManyToManyField(through='core.VideoPlayList', to='core.Video'),
        ),
    ]
