from django.contrib import admin
from apps.core import views
from django.conf.urls import patterns, url, include
from rest_framework.routers import DefaultRouter

# Django REST Framework URLs
router = DefaultRouter()
router.register(r'videos', views.VideoViewSet)
router.register(r'playlist', views.PlayListViewSet)
router.register(r'videoplaylist', views.VideoPlayListViewSet)




urlpatterns = [
    # Examples:
    # url(r'^$', 'playvideos.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
